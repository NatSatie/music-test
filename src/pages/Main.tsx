/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/button-has-type */
/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
/* eslint-disable @typescript-eslint/naming-convention */
import { useEffect, useState, useCallback, useMemo, useRef } from 'react'
import axios from '../axios/axios'
import getTokenInfo from '../common/getTokenInfo'
import CurrentSong from '../components/CurrentSong'

/* eslint-disable react/react-in-jsx-scope */
const Main = (): JSX.Element => {
  const [spotifyPlayer, setSpotifyPlayer] = useState<any>(undefined)
  const [isActive, setIsActive] = useState<boolean>(true)
  const [isPaused, setIsPaused] = useState<boolean>(true)
  const [positionTrack, setPositionTrack] = useState<number>(0)
  const [durationTrack, setDurationTrack] = useState<number>(0)
  const currentSongTrack = useRef<any>(undefined)

  useEffect(() => {
    const spotifyScript = document.createElement('script')
    spotifyScript.src = 'https://sdk.scdn.co/spotify-player.js'
    spotifyScript.async = true

    document.body.appendChild(spotifyScript)

    window.onSpotifyWebPlaybackSDKReady = () => {
      const token = getTokenInfo().accessToken
      const player = new Spotify.Player({
        name: 'Web Playback SDK Quick Start Player',
        getOAuthToken: (cb) => { cb(token) },
        volume: 0.5
      })

      setSpotifyPlayer(player)

      // Ready
      // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression, @typescript-eslint/no-misused-promises
      void player.addListener('ready', async ({ device_id }) => {
        console.log('Ready with Device ID', device_id)
        axios.transferPlayback([device_id])
        setIsActive(true)
      })

      // Not Ready
      player.addListener('not_ready', ({ device_id }) => {
        console.log('Device ID has gone offline', device_id)
      })

      player.addListener('initialization_error', ({ message }) => {
        console.error(message)
      })

      player.addListener('authentication_error', ({ message }) => {
        console.error(message)
      })

      player.addListener('account_error', ({ message }) => {
        console.error(message)
      })

      player.addListener('player_state_changed', ({
        position,
        duration,
        track_window
      }) => {
        if (position !== null &&
          duration !== null &&
          track_window !== null) {
          console.log(
            'player_state_changed tracker'
          )
          console.log(
            position,
            duration,
            track_window,
            track_window.current_track
          )
          setPositionTrack(position)
          setDurationTrack(duration)
          currentSongTrack.current = track_window
        }
      })

      const togglePlay = document.getElementById('togglePlay')

      if (togglePlay != null) {
        togglePlay.onclick = async () => {
          await player.togglePlay()
        }
      }

      void player.connect()
    }

    window.onbeforeunload = () => {
      spotifyPlayer.disconnect()
      setIsPaused(true)
      setIsActive(false)
    }
  }, [])

  useEffect(() => {
    console.log('currentSongTrack---> ', currentSongTrack)
  }, [currentSongTrack])

  const handlePlayPause = useCallback(() => {
    spotifyPlayer.togglePlay()
    setIsPaused(!isPaused)
    // void spotifyPlayer.getCurrentState().then((state: any) => {
    //   setPositionTrack(state?.position)
    //   setDurationTrack(state?.duration)
    //   setCurrentSongTrack(state?.track_window ?? undefined)
    // })
  }, [spotifyPlayer])

  return (
    <>
      <h1>Hello Spotify user</h1>
      <button onClick={() => { axios.getFiveBestTracks() }}>
        Get your five best songs
      </button>
      {
        isActive && (
        <>
          { currentSongTrack.current !== undefined && <CurrentSong info={currentSongTrack.current as Spotify.PlaybackTrackWindow} />}
          <button className="" onClick={handlePlayPause}>
            { isPaused ? 'PLAY' : 'PAUSE' }
          </button>
        </>
        )
      }
    </>
  )
}

export default Main
