/* eslint-disable react/button-has-type */
/* eslint-disable no-multiple-empty-lines */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import React, { useCallback } from 'react'
import generateUrl from '../common/generateUrl'


// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const Auth = (): JSX.Element => {
  const handleClick = useCallback(() => {
    window.location.replace(generateUrl())
  }, [])

  return (
    <button onClick={handleClick}>Log in with Spotify account</button>
  )
}

export default Auth
