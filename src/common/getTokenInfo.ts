/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
// http://localhost:3000/main#access_token=BQA2fkwdUMJWaLkadJ1__gQiUCsoBHFo5Ce9JxT5EpucteqSF0ING7BwzwrWzjPoO6Y9cpR7wOf35DyINpEt2AkzF3EDd0YLv0fQED0tpuqpuu5vRP0ENi4Y9byF76LqSVI1KufSWXtfqkwb5lch2mvOiZjewR6QJWEJINBhR07CeJmr3w8vF5hqmzN26YJrYLgdmw6kJ7ushM0dOLgxlZ4zltMw_lTFGG0TfPkCkXBm1qrQLg2rViZqFTnZrg7A3uTH13GTmC7GFK7-PM2XCXD3HS-xMqdat6ZtVY_p_OOI9UjCnQpTLkEuyMD_&token_type=Bearer&expires_in=3600

import Config from '../constants/config'

export interface TokenInfo {
  accessToken: string
  expiresIn: number
  tokenType: string
}

const removeRedirectUrl = (params: string): string => (
  params.split(`${Config.redirectUrl ?? ''}#`)[1]
)

const getTokenInfo = (): TokenInfo => {
  const params = window.location.href
  const accessToken = removeRedirectUrl(params)
    .split('&')[0].split('access_token=')[1]
  const tokenType = removeRedirectUrl(params)
    .split('&')[1].split('token_type=')[1]
  const expiresIn = parseInt(removeRedirectUrl(params)
    .split('&')[2].split('expires_in=')[1], 10)
  return { accessToken, expiresIn, tokenType }
}

export default getTokenInfo
