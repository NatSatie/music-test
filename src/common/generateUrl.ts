/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import Config from '../constants/config'
import Scopes from '../constants/scopes'

const authEndpoint = 'https://accounts.spotify.com/authorize'

const { redirectUrl, clientId } = Config

const getAllScopes = (): string => Object
  .values(Scopes)
  .reduce((result, scope) => result.concat(`${scope}%20`), '')

// eslint-disable-next-line @typescript-eslint/no-unused-vars, max-len
const url = `${authEndpoint}?client_id=${clientId ?? ''}&redirect_uri=${redirectUrl ?? ''}&scope=${getAllScopes()}&response_type=token&show_dialog=true`

const generateUrl = (): string => url

export default generateUrl
