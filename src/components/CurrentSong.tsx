/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-len */
/* eslint-disable react/button-has-type */
/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
/* eslint-disable @typescript-eslint/naming-convention */
import { useMemo } from 'react'

interface CurrentSongProps {
  info: Spotify.PlaybackTrackWindow
}

/* eslint-disable react/react-in-jsx-scope */
const CurrentSong = ({ info }: CurrentSongProps): JSX.Element => {
  console.log('>> Current song here << ')
  console.log(info)
  console.log(Object.keys(info))

  console.log('current_track artists')
  console.log(info)

  const artists = useMemo(
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    (): string => info?.current_track?.artists?.reduce((result: string, artist: { name: string }) => result.concat(`${artist.name} `), '')
    , [info]
  )

  const name = useMemo(
    (): string => info?.current_track?.name
    , [info]
  )

  return (
    <>
      <h1>{artists}</h1>
      <h1>{name}</h1>
    </>
  )
}

export default CurrentSong
