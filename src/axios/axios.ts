/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import getSongTrack from './getSongTrack'
import getFiveBestTracks from './getFiveBestTracks'
import getPlaybackState from './getPlaybackState'
import getCurrentSongTrack from './getCurrentSongTrack'
import transferPlayback from './transferPlayback'

export default {
  getSongTrack,
  getFiveBestTracks,
  getPlaybackState,
  getCurrentSongTrack,
  transferPlayback
}
