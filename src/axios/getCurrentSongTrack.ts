/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import axios from 'axios'
import getTokenInfo from '../common/getTokenInfo'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getCurrentSongTrack = (): any => {
  axios({
    method: 'GET',
    // eslint-disable-next-line max-len
    url: 'https://api.spotify.com/v1/me/player/currently-playing',
    headers: {
      Authorization: `Bearer ${getTokenInfo().accessToken ?? ''}`,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    data: {
      // TODO: change the market acconding to the location of the PC
      market: 'BR'
    }
  }).then((response) => {
    console.log(response)
    return response
  }).catch((error) => {
    // TODO: remove console error and change to a toast or something
    console.error(error)
  })
}

export default getCurrentSongTrack
