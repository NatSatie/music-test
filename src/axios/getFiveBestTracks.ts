/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import axios from 'axios'
import getTokenInfo from '../common/getTokenInfo'

const getFiveBestTracks = (): void => {
  axios({
    method: 'GET',
    // eslint-disable-next-line max-len
    url: 'https://api.spotify.com/v1/me/top/tracks?time_range=short_term&limit=5',
    headers: {
      Authorization: `Bearer ${getTokenInfo().accessToken ?? ''}`,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }).then((response) => {
    console.log(response)
  }).catch((error) => {
    console.error(error)
  })
}

export default getFiveBestTracks
