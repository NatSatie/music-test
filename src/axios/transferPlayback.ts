/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import axios from 'axios'
import getTokenInfo from '../common/getTokenInfo'

const transferPlayback = (deviceId: string[]): void => {
  axios({
    method: 'PUT',
    url: 'https://api.spotify.com/v1/me/player',
    headers: {
      Authorization: `Bearer ${getTokenInfo().accessToken ?? ''}`,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    data: {
      device_ids: deviceId
    }
  }).then((response) => {
    console.log(response)
  }).catch((error) => {
    console.log('Error at transfer playback')
    console.error(error)
  })
}

export default transferPlayback
