/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import axios from 'axios'
import getTokenInfo from '../common/getTokenInfo'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getPlaybackState = (): any => {
  axios({
    method: 'GET',
    // eslint-disable-next-line max-len
    url: 'https://api.spotify.com/v1/me/player',
    headers: {
      Authorization: `Bearer ${getTokenInfo().accessToken ?? ''}`,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    data: {
      market: 'BR'
    }
  }).then((response) => response).catch((error) => {
    console.error(error)
  })
}

export default getPlaybackState
