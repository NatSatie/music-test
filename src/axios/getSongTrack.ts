/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import axios from 'axios'
import getTokenInfo from '../common/getTokenInfo'

const getSongTrack = (): void => {
  const songId = '58YF9ZJIfMO1wcZrklmnA4'
  axios({
    method: 'GET',
    url: `https://api.spotify.com/v1/tracks/${songId}`,
    headers: {
      Authorization: `Bearer ${getTokenInfo().accessToken ?? ''}`,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }).then((response) => {
    console.log(response)
  }).catch((error) => {
    console.error(error)
  })
}

export default getSongTrack
