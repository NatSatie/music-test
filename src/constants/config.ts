// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config()

const { REACT_APP_CLIENT_ID, REACT_APP_REDIRECT_URL } = process.env

const Config = {
  clientId: REACT_APP_CLIENT_ID,
  redirectUrl: REACT_APP_REDIRECT_URL
}

export default Config
