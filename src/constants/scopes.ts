// eslint-disable-next-line no-shadow
enum Scopes {
  // images
  UgcImageUpload = 'ugc-image-upload',

  // follow
  UserFollowRead = 'user-follow-read',
  UserFollowModify = 'user-follow-modify',

  // listening history
  UserReadRecentlyPlayed = 'user-read-recently-played',
  UserTopRead = 'user-top-read',
  UserReadPlaybackPosition = 'user-read-playback-position',

  // library
  UserLibraryRead = 'user-library-read',
  UserLibraryModify = 'user-library-modify',

  // spotify connect
  UserReadPlaybackState = 'user-read-playback-state',
  UserReadCurrentlyPlaying = 'user-read-currently-playing',
  UserModifyPlaybackState = 'user-modify-playback-state',

  // playlists
  PlaylistReadCollaborative = 'playlist-read-collaborative',
  PlaylistModifyPrivate = 'playlist-modify-private',
  PlaylistModifyPublic = 'playlist-modify-public',
  PlaylistReadPrivate = 'playlist-read-private',

  // playback
  Streaming = 'streaming',
  AppRemoteControl = 'app-remote-control',

  // users
  UserReadEmail = 'user-read-email',
  UserReadPrivate = 'user-read-private'
}

export default Scopes
